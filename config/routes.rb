Rails.application.routes.draw do
    root to: 'registers#index'

    # database/api
    get 'register/:id', to: 'registers#index'
    post 'register', to: 'registers#create'
    put 'register/:id', to: 'registers#update'
    delete 'register/:id', to: 'registers#destroy'

    # api/front-end
    post 'api/query', to: 'api#query'
    post 'api/group', to: 'api#group'
    post 'api/geolocation', to: 'api#geolocation'
end
